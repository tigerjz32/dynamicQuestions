<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="/bootstrap/js/bootstrap.min.js"></script>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>Commeff Questions</title>
</head>

<body>
	<ul class="nav nav-tabs nav-justified">
		<li class="active"><a href="user.jsp">Users</a></li>
		<li><a href="userlist.jsp">UserList</a></li>
		<li><a href="login.jsp">Login</a></li>
	</ul>

	<form style="margin: 5% 5%; width: 20%" align="center" type="GET" action="commeff_questions">
		<input type="hidden" name="value" value="createUser">
		<input class="form-control" name="firstname" placeholder="First Name"> <br/>
		<input class="form-control" name="lastname" placeholder="Last Name"> <br/>
		<input class="form-control" name="email" placeholder="Email"> <br/>
		<input class="form-control" type="password" name="password" placeholder="Password"> <br/>
		<input class="form-control" name="institution" placeholder="Institution"> <br/>
		<table>
			<tr><td>
				<input type="radio" name="role" value="student">
			</td><td>
				Student
			</td></tr><tr><td>
				<input type="radio" name="role" value="instructor">
			</td><td>
				Instructor
			</td></tr>
		</table>
		<button type="submit" class="btn btn-default">Submit</button>
	</form>

</body>
</html>
