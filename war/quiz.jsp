<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<%@ page import="java.util.List"%>
<%@ page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@ page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@ page import="com.google.appengine.api.datastore.Query"%>
<%@ page import="com.google.appengine.api.datastore.PreparedQuery"%>
<%@ page import="com.google.appengine.api.datastore.Entity"%>
<%@ page import="com.google.appengine.api.datastore.FetchOptions"%>
<%@ page import="com.google.appengine.api.datastore.Key"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>

<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="/bootstrap/js/bootstrap.min.js"></script>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>Commeff Questions</title>
</head>

<body>
	<ul class="nav nav-tabs nav-justified">
		<li><a href="icontent.jsp">Content</a></li>
		<li><a href="icontentlist.jsp">ContentList</a></li>
		<li class="active"><a href="quiz.jsp">Quiz</a></li>
		<li><a href="quizlist.jsp">QuizList</a></li>
		<li><a href="scale.jsp">Scale</a></li>
	</ul>

	<form style="margin: 5% 5%;" type="GET" action="commeff_questions">
		<input type="hidden" name="value" value="createQuiz">
		<div class="radio">
			<%
			DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			Query query = new Query("Content").addFilter("approved", Query.FilterOperator.EQUAL,"true");
			PreparedQuery pq = datastore.prepare(query);
			for (Entity c : pq.asIterable()) {
			String[] options = c.getProperty("options").toString().split("\\n");
			int counter = 1;
			%>
			<label> <input name="questions" type="radio"
				value="<%=c.getKey().getId()%>"> <%=c.getProperty("question")%>
				<br /> <% for(String s : options)  { %> &nbsp;&nbsp;&nbsp;<%=counter%>
				<%=s%> <br /> <%
						counter++;
					} %> <br />
			</label>
			<% 
			}
		%>
		</div>
		<button type="submit" class="btn btn-default">Submit</button>
	</form>
</body>
</html>
