<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>

<%@ page import="java.util.List"%>
<%@ page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@ page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@ page import="com.google.appengine.api.datastore.Query"%>
<%@ page import="com.google.appengine.api.datastore.PreparedQuery"%>
<%@ page import="com.google.appengine.api.datastore.Entity"%>
<%@ page import="com.google.appengine.api.datastore.FetchOptions"%>
<%@ page import="com.google.appengine.api.datastore.Key"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>

<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<!-- Optional theme -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.css">
<!-- Latest compiled and minified JavaScript -->
<script src="/bootstrap/js/bootstrap.js"></script>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>Commeff Questions</title>
</head>

<body>
	<ul class="nav nav-tabs nav-justified">
		<li><a href="icontent.jsp">Content</a></li>
		<li><a href="icontentlist.jsp">ContentList</a></li>
		<li><a href="quiz.jsp">Quiz</a></li>
		<li><a href="quizlist.jsp">QuizList</a></li>
		<li class="active"><a href="scale.jsp">Scale</a></li>
	</ul>

	<form role="form" style="margin: 5% 5%; width: 20%;" align="center"
		type="GET" action="commeff_questions">
		
		<input type="hidden" name="value" value="changeScale"> 

		<textarea rows="4" cols="50" name="scale" placeholder="TITLE, WEIGHT, press ENTER after each"></textarea>
		<br /><br />
		
		<button type="submit" class="btn btn-default">Submit</button>
	</form>
</body>
</html>
