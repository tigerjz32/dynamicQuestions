<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>

<%@ page import="java.util.List"%>
<%@ page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@ page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@ page import="com.google.appengine.api.datastore.Query"%>
<%@ page import="com.google.appengine.api.datastore.PreparedQuery"%>
<%@ page import="com.google.appengine.api.datastore.Entity"%>
<%@ page import="com.google.appengine.api.datastore.FetchOptions"%>
<%@ page import="com.google.appengine.api.datastore.Key"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>

<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<!-- Optional theme -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.css">
<!-- Latest compiled and minified JavaScript -->
<script src="/bootstrap/js/bootstrap.js"></script>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>Commeff Questions</title>
</head>

<body>
	<ul class="nav nav-tabs nav-justified">
		<li class="active"><a href="icontent.jsp">Content</a></li>
		<li><a href="icontentlist.jsp">ContentList</a></li>
		<li><a href="quiz.jsp">Quiz</a></li>
		<li><a href="quizlist.jsp">QuizList</a></li>
		<li><a href="scale.jsp">Scale</a></li>
	</ul>

	<form role="form" style="margin: 5% 5%; width: 20%;" align="center"
		type="GET" action="commeff_questions">
		
		<input type="hidden" name="value" value="createContent"> 
		<input type="hidden" name="approved" value="true"> 
		<input class="form-control" id="content" name="question" placeholder="Question...?">
			
		<br /><br />
		<textarea rows="4" cols="50" name="options" placeholder="Options, press ENTER after each option..."></textarea>
		<br /><br />
		<textarea rows="4" cols="50" name="answer" placeholder="Answer..."></textarea>
		<br /><br />
		<textarea rows="4" cols="50" name="reason" placeholder="Reason..."></textarea>
		<br /><br />
		<table>
			<tr>
				<td><input type="radio" name="access" value="public"></td>
				<td>Public</td>
			</tr>
			<tr>
				<td><input type="radio" name="access" value="private">
				</td>
				<td>Private</td>
			</tr>
		</table>
		<br />
		<select name="instructor">
		<option value="" disabled selected>Instructor...</option>
		<%
			DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			Query query = new Query("User");
			PreparedQuery pq = datastore.prepare(query);
			for (Entity u : pq.asIterable()) {
				if(u.getProperty("role").equals("instructor")) {
		%>
			<option value="<%=u.getKey()%>"><%=u.getProperty("firstname")%> <%=u.getProperty("lastname")%> - <%=u.getProperty("institution")%></option>
		<%
				}
			}
		%>
		</select>

		<br />
		<hr>
		
		<h2>Tagging:</h2>
		
		<!-- Could be queried from a database, maybe we could also use class names here? -->
		<select name="subject">
			<option value="" disabled selected>Subject...</option>
			<option value="english">English</option>
			<option value="math">Math</option>
			<option value="science">Science</option>
		</select>
		
		<br /><br />
		
		<select name="difficulty">
		<option value="" disabled selected>Difficulty...</option>
		<%
			query = new Query("Scale");
			pq = datastore.prepare(query);
			for (Entity e : pq.asIterable()) {
		%>
			<option value="<%=e.getProperty("scaleNumber")%>"><%=e.getProperty("scaleTitle")%></option>
		<%
			}
		%>
		</select>
		
		<br /><br />
		
		<!-- To offer wide range of tags -->
		<textarea rows="4" cols="50" name="tags" placeholder="Tags... seperate by comma"></textarea>
		<br /><br />
		
		<button type="submit" class="btn btn-default">Submit</button>
	</form>
</body>
</html>
