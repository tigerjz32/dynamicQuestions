<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<html>

<%@ page import="java.util.List"%>
<%@ page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@ page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@ page import="com.google.appengine.api.datastore.Query"%>
<%@ page import="com.google.appengine.api.datastore.PreparedQuery"%>
<%@ page import="com.google.appengine.api.datastore.Entity"%>
<%@ page import="com.google.appengine.api.datastore.FetchOptions"%>
<%@ page import="com.google.appengine.api.datastore.Key"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>

<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
<!-- Optional theme -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.css">
<!-- Latest compiled and minified JavaScript -->
<script src="/bootstrap/js/bootstrap.js"></script>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>Commeff Questions</title>
</head>

<body>
	<ul class="nav nav-tabs nav-justified">
		<li><a href="content.jsp">Content</a></li>
		<li><a href="contentlist.jsp">ContentList</a></li>
		<li class="active"><a href="takequiz.jsp">Quiz</a></li>
	</ul>
	<center>
		<h2>Quiz:</h2>	
		<form style="margin: 5% 5%;" type="GET" action="commeff_questions">
			<input type="hidden" name="value" value="takeQuiz">
			<button type="submit" class="btn btn-success">Start</button>
		</form>
	</center>
</body>
</html>
