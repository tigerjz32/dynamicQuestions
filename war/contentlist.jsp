<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->
<%@ page import="java.util.List"%>
<%@ page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@ page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@ page import="com.google.appengine.api.datastore.Query"%>
<%@ page import="com.google.appengine.api.datastore.PreparedQuery"%>
<%@ page import="com.google.appengine.api.datastore.Entity"%>
<%@ page import="com.google.appengine.api.datastore.FetchOptions"%>
<%@ page import="com.google.appengine.api.datastore.Key"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>

<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="/bootstrap/js/bootstrap.min.js"></script>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<title>Commeff Questions</title>
</head>

<body>
	<ul class="nav nav-tabs nav-justified">
		<li><a href="content.jsp">Content</a></li>
		<li class="active"><a href="contentlist.jsp">ContentList</a></li>
		<li><a href="takequiz.jsp">Quiz</a></li>
	</ul>
	
	
	<%
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query query = new Query("Content").addFilter("saccess", Query.FilterOperator.EQUAL, "public");
		PreparedQuery pq = datastore.prepare(query);
		for (Entity c : pq.asIterable()) {
	%>
	<%=c.getProperties()%>
	<br />
	<%
		}
	%>
	
</body>
</html>
