package com.commeff;

public class User {
	private String firstName;
	private String lastName;
	private String password;
	private String institution;
	private String email;
	private boolean instructor;
	
	public User(String a, String b, String c, String d, String e, boolean r) {
		this.firstName = a;
		this.lastName = b;
		this.password = c;
		this.institution = d;
		this.email = e;
		this.instructor = r;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setInstructor() {
		this.instructor = true;
	}
	
	public void setStudent() {
		this.instructor = false;
	}
	
	public boolean getInstructor() {
		return this.instructor;
	}
}
