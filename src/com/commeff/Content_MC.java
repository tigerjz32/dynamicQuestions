package com.commeff;

import java.util.*;

public class Content_MC {

	private String question;
	private String answer;
	private String difficulty;
	private ArrayList<String> options = new ArrayList<String>();
	
	public Content_MC(String a, String b, String c, String d) {
		setQuestion(a);
		setAnswer(b);
		setOptions(c);
		setDifficulty(d);
	}
	
	public void setDifficulty(String d) {
		difficulty = d;
	}

	public String getDiffucilty() {
		return difficulty;
	}
	
	public String getQuestion() {
		return question;
	}
	
	public void setQuestion(String question) {
		this.question = question;
	}
	
	public String getAnswer() {
		return answer;
	}
	
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	public ArrayList<String> getOptions() {
		return options;
	}
	
	public void setOptions(String options) {
		String[] s = options.split("\\n");
		for(String temp : s) {
			this.options.add(temp.trim());
		}
	}
	
	public boolean correct(String chosen) {
		if(chosen.equals(getAnswer())) {
			return true;
		} else {
			return false;
		}
	}
}
