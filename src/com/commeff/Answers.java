package com.commeff;

import java.util.HashMap;

public class Answers {

	private HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
	private int right = 0;
	private int current = 0;
	
	public Answers() {
	}
	
	public void correct(int dif) {
		if(hm.containsKey(dif)) {
			int temp = hm.get(dif);
			temp++;
			hm.put(dif, temp);
		} else {
			hm.put(dif, 1);
		}
		current += dif;
		right += dif;
	}
	
	public void wrong(int dif) {
		current += dif;
	}
	
	public String toString() {
		return hm.toString();
	}
	
	public int getCorrect() {
		return right;
	}
	 
	public int getCurrent() {
		return current;
	}
}
