package com.commeff;

import java.io.IOException;
import java.util.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.*;

@SuppressWarnings("serial")
public class Commeff_QuestionsServlet extends HttpServlet {

	static DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	static ArrayList<User> user = new ArrayList<User>();
	static ArrayList<Content_MC> content = new ArrayList<Content_MC>();
	static Key loggedInUser;
	static String loggedInUserRole;
	static Answers a;
	static int scaleSize;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {

		// user creation
		if(req.getParameter("value").equals("createUser")) {
			Entity userEntity = new Entity("User");
			String firstname = req.getParameter("firstname");
			String lastname = req.getParameter("lastname");
			String email = req.getParameter("email");
			String password = req.getParameter("password");
			String institution = req.getParameter("institution");
			boolean role = false;
			String srole = "student";

			if(req.getParameter("role").equals("instructor")) {
				role = true;
				srole = "instructor";
			}

			user.add(new User(firstname, lastname, email, password, institution, role));

			userEntity.setProperty("firstname", firstname);
			userEntity.setProperty("lastname", lastname);
			userEntity.setProperty("email", email);
			userEntity.setProperty("password", password);
			userEntity.setProperty("institution", institution);
			userEntity.setProperty("role", srole);
			datastore.put(userEntity);

			RequestDispatcher view = req.getRequestDispatcher("user.jsp");
			view.forward(req, resp);

			// user login
		} else if (req.getParameter("value").equals("loginUser")) {

			String email = req.getParameter("email");
			String password = req.getParameter("password");
			boolean valid1 = false;
			boolean valid2 = false;

			DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
			Query query = new Query("User");
			PreparedQuery pq = datastore.prepare(query);
			Iterator<Entity> ie = pq.asIterator();
			for (Entity u : pq.asIterable()) {
				if(u.getProperty("email").equals(email) && u.getProperty("password").equals(password)) {
					loggedInUser = u.getKey();
					loggedInUserRole = (String) u.getProperty("role");
					valid1 = true;
					if(u.getProperty("role").equals("instructor")) {
						valid2 = true;
					}
				}
			}

			if(valid1 && valid2) {
				RequestDispatcher view = req.getRequestDispatcher("icontent.jsp");
				view.forward(req, resp);
			} else if (valid1 && !valid2) {
				RequestDispatcher view = req.getRequestDispatcher("content.jsp");
				view.forward(req, resp);
			} else {
				resp.setContentType("text/plain");
				resp.getWriter().println("Invalid login");
			}

			// question creation
		} else if (req.getParameter("value").equals("createContent")) {

			String approved = req.getParameter("approved");
			String question = req.getParameter("question");
			String options = req.getParameter("options");
			String answer = req.getParameter("answer");
			String reason = req.getParameter("reason");
			String instructor = req.getParameter("instructor");
			String subject = req.getParameter("subject");
			String difficulty = req.getParameter("difficulty");
			String tags = req.getParameter("tags");
			String saccess = "private";
			String inQuiz = "false";

			if(req.getParameter("access").equals("public")) {
				saccess = "public";
			}
			Entity contentEntity = new Entity("Content");
			contentEntity.setProperty("owner", loggedInUser.getId());
			contentEntity.setProperty("approved", approved);
			contentEntity.setProperty("question", question);
			contentEntity.setProperty("options", options);
			contentEntity.setProperty("answer", answer);
			contentEntity.setProperty("reason", reason);
			contentEntity.setProperty("instructor", instructor);
			contentEntity.setProperty("saccess", saccess);
			contentEntity.setProperty("subject", subject);
			contentEntity.setProperty("difficulty", difficulty);
			contentEntity.setProperty("tags", tags);
			contentEntity.setProperty("inQuiz", inQuiz);
			datastore.put(contentEntity);

			if(loggedInUserRole.equals("instructor")) {
				RequestDispatcher view = req.getRequestDispatcher("icontent.jsp");
				view.forward(req, resp);
			} else {
				RequestDispatcher view = req.getRequestDispatcher("content.jsp");
				view.forward(req, resp);
			}

			// quiz creation
		} else if (req.getParameter("value").equals("createQuiz")) {
			String[] questions = req.getParameterValues("questions");
			Query query = new Query("Content");
			PreparedQuery pq = datastore.prepare(query);
			for(String s : questions) {
				for (Entity c : pq.asIterable()) {
					if(String.valueOf(c.getKey().getId()).equals(s)) {
						c.setProperty("inQuiz", "true");
						datastore.put(c);
					}
				}
			}
			RequestDispatcher view = req.getRequestDispatcher("quiz.jsp");
			view.forward(req, resp);

			// approve content
		} else if (req.getParameter("value").equals("approveContent")) {
			String[] questions = req.getParameterValues("questions");
			Query query = new Query("Content");
			PreparedQuery pq = datastore.prepare(query);
			for(String s : questions) {
				for (Entity c : pq.asIterable()) {
					if(String.valueOf(c.getKey().getId()).equals(s)) {
						c.setProperty("approved", "true");
						datastore.put(c);
					}
				}
			}
			RequestDispatcher view = req.getRequestDispatcher("icontentlist.jsp");
			view.forward(req, resp);

			// take quiz
		} else if (req.getParameter("value").equals("takeQuiz")) {
			Query query = new Query("Content").addFilter("inQuiz", Query.FilterOperator.EQUAL, "true");
			PreparedQuery pq = datastore.prepare(query);
			content.clear();
			for (Entity c : pq.asIterable()) {
				Content_MC temp = new Content_MC((String)c.getProperty("question"),(String)c.getProperty("answer"),(String)c.getProperty("options"),(String)c.getProperty("difficulty"));
				content.add(temp);
			}

			a = new Answers();

			String question = null;
			ArrayList<String> options = null;

			for (Content_MC c : content) {
				question = c.getQuestion();
				options = c.getOptions();
			}

			RequestDispatcher view = req.getRequestDispatcher("takeQuiz2.jsp");
			req.setAttribute("question", question);
			req.setAttribute("options", options.toArray(new String[options.size()]));
			view.forward(req, resp);

			// answer questions
		} else if (req.getParameter("value").equals("takeQuiz2")) {

			String question = req.getParameter("question");
			String selected = req.getParameter("selected");
			ArrayList<String> options = null;

			for(Content_MC c : content) {
				if(c.getQuestion().equals(question)) {
					if(c.getAnswer().equals(selected)) {
						a.correct(Integer.parseInt(c.getDiffucilty()));
						content.remove(c);
						break;
					} else {
						a.wrong(Integer.parseInt(c.getDiffucilty()));
						content.remove(c);
						break;
					}
				}
			}

			System.out.println(a.toString());
			double co = (a.getCorrect()/a.getCurrent())*100;
			for (Content_MC c : content) {
				if (co > 50) {
					if (Integer.parseInt(c.getDiffucilty()) <= 50 ) {
						question = c.getQuestion();
						options = c.getOptions();
						break;
					} else {
						question = c.getQuestion();
						options = c.getOptions();
						break;
					}
				} else {
					if (Integer.parseInt(c.getDiffucilty()) >= 50 ) {
						question = c.getQuestion();
						options = c.getOptions();
						break;
					} else {
						question = c.getQuestion();
						options = c.getOptions();
						break;
					}
				}
			}

			if(content.size() == 0) {
				String result = a.toString();

				resp.setContentType("text/plain");
				resp.getWriter().println(result);
			} else {
				RequestDispatcher view = req.getRequestDispatcher("takeQuiz2.jsp");
				req.setAttribute("question", question);
				req.setAttribute("options", options.toArray(new String[options.size()]));
				view.forward(req, resp);
			}

		} else if (req.getParameter("value").equals("changeScale")) {
			String scaleTag = req.getParameter("scale");
			ArrayList<String> title = new ArrayList<String>();
			ArrayList<String> scale = new ArrayList<String>();

			String[] s = scaleTag.split("\\n");
			for(String temp : s) {
				String t1 = temp.trim();
				String[] s2 = t1.split(",");
				title.add(s2[0].trim());
				scale.add(s2[1].trim());
			}
			scaleSize = scale.size();
			for(int i = 0; i < scale.size(); i++) {
				Entity contentEntity = new Entity("Scale");
				contentEntity.setProperty("owner", loggedInUser.getId());
				contentEntity.setProperty("scaleTitle", title.get(i));
				contentEntity.setProperty("scaleNumber", scale.get(i));
				datastore.put(contentEntity);
			}

			RequestDispatcher view = req.getRequestDispatcher("icontent.jsp");
			view.forward(req, resp);
		}
	}
}
