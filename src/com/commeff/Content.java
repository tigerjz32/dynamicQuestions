package com.commeff;

public abstract class Content {
	abstract void setQuestion(String s);
	abstract void setAnswer(String s);
	abstract void setReason(String s);
	abstract void setInstructor(String s);
	
	abstract String getQuestion();
	abstract String getAnswer();
	abstract String getReason();
	abstract String getInstructor();
}
